//import thư viện mongoose
const mongoose = require("mongoose");

//import dice history model
const diceHisModel = require("../models/diceHistoryModel");

//function create dice history
const createDiceHistory = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    const newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        dice: Math.floor(Math.random() * 6) + 1,
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
    diceHisModel.create(newDiceHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new dice history successfully",
            data: data
        })
    })
}
//function get all dice history
const getAllDiceHis = (request, response) =>{
    diceHisModel.find((error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:"Get all dice histories successfully",
            data: data
        })
    })
}

//get dice history by id
const getDiceHisById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const diceHisId = request.params.diceHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHisId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Dice history id không hợp lệ"
        })
    }
    //B3: gọi model chứa id cần tìm
    diceHisModel.findById(diceHisId, (error,data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            message:`Get dice history with id ${diceHisId} successfully`,
            data: data
        })
    })
}

//function update dice history by id
const updateDiceHisById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const diceHisId = request.params.diceHisId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHisId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Dice history Id không đúng"
        })
    }
    if(isNaN(body.dice) || body.dice < 0 && body.dice > 6){
        return response.status(400).json({
            status: "Bad Request",
            message:"dice không hợp lệ"
        })
    }
   
    //B3: gọi model chứa id để tìm và update dữ liệu
    const updateDiceHis = {};
    if(body.user !== undefined){
        updateDiceHis.user = body.user
    }
    if(body.dice !== undefined){
        updateDiceHis.dice = body.dice
    }

    diceHisModel.findByIdAndUpdate(diceHisId, updateDiceHis, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update dice history with id ${diceHisId} successfully`,
            data: data
        })
    })
}
//function delete dice history by id
const deleteDiceHisById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const diceHisId = request.params.diceHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHisId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Dice History id không đúng"
        })
    }
    //B3: gọi dice history model chưa id cần xóa
    diceHisModel.findByIdAndRemove(diceHisId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Dice history had id ${diceHisId} successfully`
        })
    })
}
module.exports = {
    createDiceHistory,
    getAllDiceHis,
    getDiceHisById,
    updateDiceHisById,
    deleteDiceHisById
}